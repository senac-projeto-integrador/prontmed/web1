Como rodar o projeto baixado

Sequência executada e bibliotecas instaladas durante a Semana de Imersão

Criar o arquivo package.json
### npm init

Instalar o react e o next [https://nextjs.org/docs/getting-started](https://nexjs.org/docs/getting-started)
### npm install --save next react react-dom

Abra o arquivo package.json e adicione o seguinte script:
"scripts": {
    "dev": "next",
    "build": "next build",
    "start": "next start"
  }

Rodar o projeto
### npm run dev

Abrir o projeto endereço padrão
[http://localhost:3000](http://localhost:3000)

Instalar o Bootstrap
### npm install --save bootstrap

Instalar o Reactstrap
### npm install --save reactstrap

Instalar a biblioteca para inserir o CSS diretamente no HTML. [https://github.com/zeit/next-plugins/tree/master/packages/next-css](https://zeit/next-plugins/tree/master/packages/next-css)
### npm install --save @zeit/next-css

Instalar os ícones 
### npm install --save @fortawesome/fontawesome-svg-core @fortawesome/free-solid-svg-icons @fortawesome/react-fontawesome

Realizar conexão com a API
### npm install --save axios




